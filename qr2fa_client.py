#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

import string
import random
import configparser
import os.path
import sys
import requests
import json
import qrcode
import urllib3
import time
from typing import Any, NoReturn

def genLoginId(length: int) -> str:
    """
        General egy hosszu random stringet

        :param length: Ilyen hosszusagu stringet fog generalni
        :return: 'length' hosszusagu string
    """

    letters = string.ascii_letters

    return ''.join(random.choice(letters) for x in range(length))

def genAuthCodes(num: int) -> int:
    """
        General nehany veletlen szamot 100000 es 999999 kozott.
        Egy tombel ter vissza, amiben a num szamu veletlenszam talalhato

        :param num: Ennyi veletlenszamot general
        :return: 'num' szamu veletlenszam tombje
    """

    rnd = []
    for x in range(num):
        rnd.append(random.choice(range(100000, 999999)))

    return rnd

def getOneAuthCode(codes: list) -> str:
    """
        Kivalaszt egy szamot a codes list -bol

        :param codes: A genAuthCodes altal generalt lista
        :return: a kivalasztott auth code
    """

    return random.choice(codes)

def setAuth(url: str, code: str, auth_codes: list) -> dict:
    """
        POST keresben kuldi az auth serverre a bejelentkezo adatokat

        :param url: A teljes urlje (a login_id -val egyutt) annak a címnek ahova a POST kerest el kell kuldeni
        :param code: A helyes authentikacios kod
        :param auth_codes: Tomb, amiben az osszes megjelenitendo kod benne van
        :retutn: A vissza kapott JSON -nal ter vissza
    """

    # Csak teszteleshez
    urllib3.disable_warnings()

    post_message = {'action': 'set_login', 'auth_codes': auth_codes, 'good_choice': code}
    headers = { 'content-type': "application/json", 'cache-control': "no-cache" }
    response = requests.request("POST", url, data = json.dumps(post_message), headers = headers, verify = False)
    response_json = json.loads(response.json())

    if response.status_code != 200:
        raise AttributeError("Auth response status must 200")

    if response_json['auth_state'] != 'initialised':
        raise AttributeError("Auth state not in initialised state")

    return response_json

def getAuth(url: str) -> str:
    """
        POST kérésben kérdezi le az autentikacio allapotat

        :param url: A teljes urlje (a login_id -val egyutt) annak a címnek ahova a POST kerest el kell kuldeni
        :return: A valaszban visszakapott auth_state ertekevel ter vissza
    """

    post_message = {'action': 'get_login'}
    headers = { 'content-type': "application/json", 'cache-control': "no-cache" }
    response = requests.request("POST", url, data = json.dumps(post_message), headers = headers, verify = False)
    response_json = json.loads(response.json())

    if response.status_code != 200:
        raise AttributeError("Auth response status must 200")

    return response_json['auth_state']

def genQR(url: string) -> NoReturn:
    """
        Megjeleníti az loginurl -t QR code -ban

        :param url: A teljes urlje (a login_id -val egyutt) annak a címnek ahova a felhasznalot kell iranyitani
    """

    qr = qrcode.QRCode(error_correction=qrcode.ERROR_CORRECT_L)
    qr.add_data(url)
    qr.print_ascii(tty=True)

def getVar(config_obj: configparser.ConfigParser, config_param: str, default_param: str = '', config_section: str = "Defaults") -> Any:
    """
        Vissza adja egy valtozo ini file beli erteket, vagy egy default erteket.
        Ha egyik sem all rendelkezesre, hibat ad

        :param config_param: Az ini fileban talalhato parameter neve
        :param default_param: Az ini fileban talalhato parameter neve, amiben az alapertelmezett ertek talalhato
        :param config_section: Az ini file szekcio neve, ahonnan a parameterek jonnek
        :return: Parameter ertek
    """

    if config_param in config_obj[config_section].keys():
        return config_obj[config_section][config_param]
    else:
        if default_param in config_obj[config_section].keys():
            return config_obj[config_section][default_param]
        else:
            raise RuntimeError(config_param + " or " + default_param + " is not defined in " + config_section)


if __name__ == '__main__':
    conf = configparser.ConfigParser()
    configfile = ''

    if os.path.isfile('/etc/qr2fa.ini'):
        configfile = '/etc/qr2fa.ini'
    elif os.path.isfile('./qr2fa.ini'):
        configfile = "./qr2fa.ini"
    else:
        sys.stderr.write("The qr2fa.ini doesn't exists!\n")
        sys.exit(127)

    conf.read(configfile)

    LOGIN_ID = genLoginId(64)
    AUTH_CODES = genAuthCodes(6)
    CODE = getOneAuthCode(AUTH_CODES)
    LOGIN_URL = getVar(conf, 'login_proto') + '://' + getVar(conf,'login_server') + ':' + getVar(conf,'login_port') + '/' + getVar(conf,'login_uri') + '?' + getVar(conf,'login_var') + '=' + LOGIN_ID
    resp = setAuth(LOGIN_URL, CODE, AUTH_CODES)
    auth = resp['auth_state']

    AUTH_URL = getVar(conf, 'auth_proto', 'login_proto') + '://' + getVar(conf, 'auth_server', 'login_server') + ':' + getVar(conf, 'auth_port', 'login_port') + '/' + getVar(conf, 'auth_uri', 'login_uri') + '?' + getVar(conf, 'auth_var', 'login_var') + '=' + LOGIN_ID
    genQR(AUTH_URL)

    while auth != 'success':
        auth = getAuth(AUTH_URL)
        print(auth)
        time.sleep(5)
