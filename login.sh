#!/usr/bin/env bash

LOGINSERVER="https://localhost"
LOGINURI="/login?q="
SMSSERVER="https://localhost/sms"
SMSSERVER_USER="abcd"
SMSSERVER_PASS="Abrakadabra1!"
LOCAL_IP="127.0.0.1"
LOCAL_HOSTNAME="localhost"
CACERT_FILE="/etc/ssl/ca-cert.pem"
MAX_WAIT_SEC=300

function exitProgram() {
  ##
  ##  Hiba uzenettel kilepes a programbol
  ##
  local MSG=$1
  local EXIT=$2

  echo "$MSG" 1>&2
  exit ${EXIT}
}

##==================================================##
##                                                  ##
##    A scriptben hasznalando parancsok             ##
##    valtozokka alakitva. Csak egy kis plusz       ##
##    biztonsagot ad, az altal, hogy megelozi       ##
##    a rendszer nehany parancsara beinjectalt      ##
##    alias lefutasat                               ##
##                                                  ##
##==================================================##
# curl full path
CURL="`/usr/bin/which curl`"
[ "${CURL}" == "" ] && exitProgram "Fuggoseg hianyzik: curl" 127

# qrencode full path
QRE="`/usr/bin/which qrencode`"
[ "${QRE}" == "" ] && exitProgram "Fuggoseg hianyzik: qrencode" 127

# pwgen full path
PWG="`/usr/bin/which pwgen`"
[ "${PWG}" == "" ] && exitProgram "Fuggoseg hianyzik: pwgen" 127

# getent full path
GETENT="`/usr/bin/which getent`"
[ "${GETENT}" == "" ] && exitProgram "Fuggoseg hianyzik: getent" 127

# cut full path
CUT="`/usr/bin/which cut`"
[ "${CUT}" == "" ] && exitProgram "Fuggoseg hianyzik: cut" 127

# shuf full path
SHUF="`/usr/bin/which shuf`"
[ "${SHUF}" == "" ] && exitProgram "Fuggoseg hianyzik: shuf" 127

# tr full path
TR="`/usr/bin/which tr`"
[ "${TR}" == "" ] && exitProgram "Fuggoseg hianyzik: tr" 127

##==================================================##
##                                                  ##
##    Fuggveny definiciok ennek meg kell eloznie    ##
##    minden futasi lepest                          ##
##                                                  ##
##==================================================##

function sendSMS(){
  ##
  ##  Elkuldi SMS -ben a bejelentkezo felhasznalonak
  ##  azt a hatjegyu kodot amire kattintania kell a
  ##  bejelentkezeshez. Ezt ugy tudja megtenni, hogy
  ##  a kigeneralt qrcode egy urlt tartalmaz, amit
  ##  ha megnyit 6 gombot talall 6 jegyu szamokkal
  ##  Itt kell arra kattintania amin az a kód szerepel
  ##  amit SMS -ben kapott
  ##
  local PAGER=$1
  local OTP=$2

  return
}

function postLoginParams() {
  ##
  ##  POST kereskent kuldi el a login oldalra a megkapott
  ##  OTP lehetosegeket es a szamot, hogy melyik helyes
  ##  A loginserver -en kell kezelni, hogy csak errol
  ##  a cimrol lehet ilyet kuldeni, vagy felhasznalonev
  ##  es jelszoval kell vedeni azt az oldalt
  ##
  local SMSRAND=$1
  local SMSSELECT=$2
  local LOGINSTRING=$3

  # Miutan
  local REC=$(${CURL} --cacert ${CACERT_FILE} --connect-timeout 3 --max-time ${MAX_WAIT_SEC} -k -sS -q --header "Content-Type: application/json" --request POST --data "{'smsrand': [${SMSRAND}], 'smsselect': ${SMSSELECT}}" ${LOGINSERVER}${LOGINURI}${LOGINSTRING})
  echo $REC

  return 1
}

function displayQRCode() {
  ##
  ##  Meg generalja a QR kodot amit le kell mobillal olvasni,
  ##  majd a linkre kattintani
  ##
  local LOGINSTRING=$1

  echo "${LOGINSERVER}${LOGINURI}${LOGINSTRING}" | ${QRE} -t UTF8
}

# Bejelentkezo user telefonszama
PAGERNUM="`${GETENT} passwd ${USER} | ${CUT} -d ':' -f5 | ${CUT} -d ',' -f3`"

# Hosszu random string a login azonositasahoz
LOGINSTRING="`${PWG} -c0s 64 1`"

# Nehany random 6 jegyu szam az azonositashoz
SMSRAND_ARRAY=( `${SHUF} -i 100000-999999 -n6 | ${TR} "\n" " "` )

# A helyes valasztas
SMSRAND_SELECT=`${SHUF} -i 0-5 -n1`

# A vesszokkel osszefuzott SMS random
SMSRAND_STRING=$(echo -en ${SMSRAND_ARRAY[@]} | ${TR} " " ",")

displayQRCode ${LOGINSTRING}
sendSMS ${PAGERNUM} ${SMSRAND_ARRAY[${SMSRAND_SELECT}]}
postLoginParams ${SMSRAND_STRING} ${SMSRAND_SELECT} ${LOGINSTRING}
