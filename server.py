#!/usr/bin/env python

# MOCK server a QR Code loginhoz

import ssl
import time
import json

from http.server import HTTPServer, BaseHTTPRequestHandler
from io import BytesIO
from urllib import parse

class SimpleHTTPRequestHandler(BaseHTTPRequestHandler):

    auth_sessions = {}

    def do_GET(self):
        q = parse.parse_qs(parse.urlparse(self.path).query).get('q', None)[0]
        self.send_response(200)
        self.end_headers()
        self.auth_sessions[q]['auth_state'] = "pending"
        self.wfile.write(str.encode(json.dumps(self.auth_sessions[q])))
        self.auth_sessions[q]['auth_state'] = "success"


    def do_POST(self):
        content_length = int(self.headers['Content-Length'])
        body = self.rfile.read(content_length)
        q = parse.parse_qs(parse.urlparse(self.path).query).get('q', None)[0]
        post_vals = json.loads(body)

        self.send_response(200)
        self.end_headers()
        response = BytesIO()

        if post_vals['action'] == "set_login":
            self.auth_sessions[q] = {"auth_codes": post_vals['auth_codes'], "good_choice": post_vals['good_choice'], "auth_state": "initialised"}
            response.write(str.encode(json.dumps('{"auth_state": "initialised"}')))
        elif post_vals['action'] == 'get_login':
            response.write(str.encode(json.dumps('{"auth_state": "' + self.auth_sessions[q]['auth_state'] + '"}')))

        self.log_message(json.dumps(self.auth_sessions))
        self.wfile.write(response.getvalue())


httpd = HTTPServer(('0.0.0.0', 8443), SimpleHTTPRequestHandler)
httpd.socket = ssl.wrap_socket (httpd.socket, certfile='/home/kayapo/Private/Development/qr2fa/ssl-cert-snakeoil.pem', keyfile='/home/kayapo/Private/Development/qr2fa/ssl-cert-snakeoil.key', server_side=True, ssl_version=ssl.PROTOCOL_TLSv1_2)
httpd.serve_forever()
